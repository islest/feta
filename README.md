# Feta Metadata Fuzzer
Feta is a Python2.7 based fuzzer for the Vorbis Comment metadata format, used
for storing media information in .ogg files. This fuzzer is written for a student project
so don't hope for windows support, etc (maybe later!)

## Dependencies:
- Requires package python-pyvorbis, which can be installed using *sudo apt-get install python-pyvorbis* on Ubuntu Systems.

## Usage:
**Using the Command Line Interface**:

*    $ *cd core* 
*    $ *./feta.py test.ogg*

The above will fuzz the file test.ogg into a directory 'feta-out' a total of one time.
Various flags can be used to specify output directory, number of times to fuzz, whether our input is a file or directory, etc.
The usage of these files can be found in *./feta.py --help*

**Using it as a Python Package**:

Easy. Just import core. This is currently in progress so expect bugs.

Licensed under Creative Commons.
