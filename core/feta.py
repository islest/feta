#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Feta.py
A media player/file format fuzzer used for fuzzing metadata tags on 
mp3 and ogg vorbis files. 

Usage:
: Takes in an input directory filled with media files with the -i tag
: Throws random stuff at their metadata tags
: Throws the new fuzzed files into the specified output directory with the
  -o tag.

Licensed under GNU License???? by Tom Isles
"""

from mutagen.oggvorbis import OggVorbis
import sys
import os
import random
import string
import optparse
from optparse import OptionParser

import junk_generator
import shell
import formats

class FileFuzzer(object):
    def __init__(self, num_repeats, input_file, output_directory):
        self._repeats = num_repeats
        self._infile = input_file
        self._output_dir = output_directory
    def run(self):
        if self._repeats == None or self._infile == None or self._output_dir == None:
            return False
        if not os.path.exists(self._output_dir):
            os.makedirs(self._output_dir)

        junk_gen = junk_generator.Selector()
        for i in range(1, self._repeats):
            with open(self._infile, 'r') as src_file:
                src_data = src_file.readlines()
                filename, extension = os.path.splitext(os.path.abspath(args[0]))
                new_filename = "{}/output-fuzzed-{}{}".format(self._output_dir,i, extension)
                print new_filename
                with open(new_filename, 'w+') as dst_file:
                    dst_file.writelines(src_data)
                
                random_junk = junk_gen.get()

                if (extension == ".ogg"):
                    metadata = formats.Vorbis(new_filename)
                    metadata.write_random_tag(random_junk)
                elif (extension == ".mp3"):
                    metadata = formats.ID3Tag(new_filename)
                    metadata.write_random_tag(random_junk)
                    
                
                    

if __name__ == "__main__":
    cli = shell.CommandLineParser()
    cli.parse_options()
    cli.validate()

    options = cli.get_options()
    args    = cli.get_args()

    num_repeats_per_file = int(options.num_repeats)
    output_directory     = os.path.abspath(options.output_directory)
    to_fuzz              = []

    if (options.read_from_dir):
        for i in os.listdir(args[0]):
            filename, extension = os.path.splitext(args[0] + "/" + i)
            if (extension == ".ogg") or (extension == ".mp3"):
                to_fuzz.append(os.path.abspath(filename + extension))
    else:
        filename, extension = os.path.splitext(os.path.abspath(args[0]))
        if (extension == ".ogg") or (extension == ".mp3"):
            to_fuzz.append(os.path.abspath(filename + extension))

    if (options.verbose):
        print
        print "=== FETA.PY - FUZZER FOR FUZZING OGG METADATA TAGS, WRITTEN BY SEACUP ==="
        print ": Gathering file settings from the arguments you gave..."
        print ": Num_Repeats = {}".format(num_repeats_per_file)
        print "    For each file you specify, {} output files will be produced.".format(num_repeats_per_file)
        print ": Output_Directory = {}".format(output_directory)
        print "    All fuzzed files will be outputted to directory {}.".format(output_directory)
        print ": Reading from a directory - " + str(options.read_from_dir)
        print ": Reading from a file - " + str(not options.read_from_dir)
        print ": Valid files exist to fuzz! We are ready to go."
        print "\n\n"
        print "=== BEGINNING FUZZ ==="

    for the_file in to_fuzz:
        the_fuzzer = FileFuzzer(num_repeats_per_file, the_file, output_directory) 
        if (options.verbose):
            print "Fuzzing {} {} times into {}".format(the_file, num_repeats_per_file,
                                                       output_directory)
        the_fuzzer.run()
