#!/usr/bin/python

from mutagen.oggvorbis import OggVorbis
import sys
import random

class Vorbis(object):
    tags = ['title', 'artist', 'album', 'genre', 'tracknumber']
    def __init__(self, filename):
        self._comment = OggVorbis(filename)
    def write_random_tag(self, data):
        tag = random.choice(Vorbis.tags)
        self._comment[tag] = data
        self._comment.save()
