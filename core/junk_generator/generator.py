#!/usr/bin/python

import random

class Generator(object):
    def __init__(self, choices=[], max_repeats=0):
        self._choices = choices
        self._range = max_repeats
    def get_value(self):
        result = ""
        for i in range(1, random.randint(1,self._range)):
            result += random.choice(self._choices)
        return result
