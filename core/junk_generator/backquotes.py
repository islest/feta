#!/usr/bin/python

from generator import Generator

class Backquotes(Generator):
    def __init__(self):
        super(Backquotes, self).__init__(['\'', '"', '\\', '`', '/'], 5000)
