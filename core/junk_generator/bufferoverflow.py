#!/usr/bin/python

from generator import Generator
import random

class BufferOverflow(Generator):
    def __init__(self):
        super(BufferOverflow, self).__init__([], 8999999)
    
    def get_value(self):
        return "A" * random.randint(1, self._range)
