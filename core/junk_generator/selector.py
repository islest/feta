#!/usr/bin/python

from bufferoverflow import BufferOverflow
from formatstring import FormatString
from randomjunk import RandomJunk
from backquotes import Backquotes
from unicode_gen import Unicode
import random

class Selector(object):
    def __init__(self):
        self._functions = [BufferOverflow(), FormatString(), RandomJunk(), Backquotes(), Unicode()]
    def get(self):
        return random.choice(self._functions).get_value()
