#!/usr/bin/python

from generator import Generator

class FormatString(Generator):
    def __init__(self):
       super(FormatString, self).__init__(['%p', '%u', '%b', '%d', '%n', '%c', '%a'],
                                          5000)

