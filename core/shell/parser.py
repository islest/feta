#!/usr/bin/python

import sys
import os
import optparse
from optparse import OptionParser

class CommandLineParser(object):
    def __init__(self):
        self._options = None
        self._args    = None
        self._parser  = OptionParser()
        
        self._parser.add_option("-d", "--directory", dest="read_from_dir",
                                action="store_true", default=False,
                                help="""Specify that we are reading multiple fuzzed
                                files from a DIRECTORY, instead of just a singular
                                file. This affects the way our file argument is read
                                and operated on.""", metavar="DIRECTORY")

        self._parser.add_option("-f", "--file", dest="read_from_dir",
                                action="store_false",
                                help="""Specify that we are reading a singular fuzzed
                                FILE, instead of multiple files from a directory.
                                This affects the way our file argument is read and
                                operated on. By default, this is set to TRUE.""",
                                metavar="FILE")

        self._parser.add_option("-o", "--output-dir", dest="output_directory",
                                default="feta_out", help="""Specify DIRECTORY_OUT as where
                                we output our fuzzed files""", metavar="DIRECTORY_OUT")

        self._parser.add_option("-n", "--num-repeats", dest="num_repeats", default=1,
                                help="Specify NUMBER as the number of times we fuzz a file.",
                                metavar="NUMBER")

        self._parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=True,
                                help="Print helpful messages! Set TRUE by default.")

        self._parser.add_option("-q", "--quiet", action="store_false", dest="verbose",
                                help="Repress helpful messages.")

    def parse_options(self):
        (self._options, self._args) = self._parser.parse_args()
    
    def validate(self):
        
        if (len(self._args) != 1):
            self._parser.error("""Feta only takes one argument - the file or directory it
                fuzzes. By default, it reads a file, but you can tell 
                it to read from a directory using -d.""")
        if (self._options.read_from_dir):
            if not os.path.isdir(os.path.abspath(self._args[0])):
                self._parser.error("""The directory you listed is not a directory! 
                Please specify a valid directory or file. """)

            dir_contains_files = False
            for i in os.listdir(self._args[0]):
                filename, extension = os.path.splitext(self._args[0] + "/" + i)
                if (extension == ".ogg"):
                    dir_contains_files = True
                    break
             
            if not dir_contains_files:
                self._parser.error("""The directory you listed doesn't contain any
                    valid files. Try with a better directory.""")
        else:
            #if os.path.isfile(os.path.abspath(self._args[0])):
            #    self._parser.error("""The file you specified does not exist or is invalid. 
            #    Try specifying an existent file or check your paths.""")
            extension = self._args[0][len(self._args[0])-4:]
            if (extension != ".ogg") and (extension != ".mp3"):
                self._parser.error("""The file you specifies isn't an .ogg or .mp3 file. Try an
                    .ogg file.""")


    def get_args(self):
        return self._args

    def get_options(self):
        return self._options
